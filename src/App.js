
import React from 'react';
import RegisterUser from './components/RegisterUser';
import LogWaterIntake from './components/LogWaterIntake';
import WaterHistory from './components/WaterHistory';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Water Reminder App</h1>
      </header>
      <main>
        <RegisterUser />
        <LogWaterIntake />
        <WaterHistory />
      </main>
    </div>
  );
}

export default App;
