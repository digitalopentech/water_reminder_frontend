
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const WaterHistory = () => {
  const [userId, setUserId] = useState('');
  const [history, setHistory] = useState([]);

  const fetchHistory = async () => {
    try {
      const response = await axios.get(`http://localhost:8000/api/v1/users/${userId}`);
      setHistory(response.data.water_intakes);
    } catch (error) {
      console.error(error);
      alert("Error fetching history.");
    }
  };

  return (
    <div>
      <h2>Water Intake History</h2>
      <div>
        <label>User ID:</label>
        <input type="number" value={userId} onChange={(e) => setUserId(e.target.value)} required />
        <button onClick={fetchHistory}>Fetch History</button>
      </div>
      <ul>
        {history.map((intake) => (
          <li key={intake.id}>
            Date: {intake.date}, Amount: {intake.amount}ml
          </li>
        ))}
      </ul>
    </div>
  );
};

export default WaterHistory;
