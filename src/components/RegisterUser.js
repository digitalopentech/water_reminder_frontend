
import React, { useState } from 'react';
import axios from 'axios';

const RegisterUser = () => {
  const [name, setName] = useState('');
  const [weight, setWeight] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post('http://localhost:8000/api/v1/users/', {
        name: name,
        weight: parseFloat(weight)
      });
      console.log(response.data);
      alert("User registered successfully!");
    } catch (error) {
      console.error(error);
      alert("Error registering user.");
    }
  };

  return (
    <div>
      <h2>Register User</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Name:</label>
          <input type="text" value={name} onChange={(e) => setName(e.target.value)} required />
        </div>
        <div>
          <label>Weight (kg):</label>
          <input type="number" value={weight} onChange={(e) => setWeight(e.target.value)} required />
        </div>
        <button type="submit">Register</button>
      </form>
    </div>
  );
};

export default RegisterUser;
