
import React, { useState } from 'react';
import axios from 'axios';

const LogWaterIntake = () => {
  const [userId, setUserId] = useState('');
  const [amount, setAmount] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post(`http://localhost:8000/api/v1/users/${userId}/water_intakes/`, {
        date: new Date().toISOString().split('T')[0],
        amount: parseFloat(amount)
      });
      console.log(response.data);
      alert("Water intake logged successfully!");
    } catch (error) {
      console.error(error);
      alert("Error logging water intake.");
    }
  };

  return (
    <div>
      <h2>Log Water Intake</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>User ID:</label>
          <input type="number" value={userId} onChange={(e) => setUserId(e.target.value)} required />
        </div>
        <div>
          <label>Amount (ml):</label>
          <input type="number" value={amount} onChange={(e) => setAmount(e.target.value)} required />
        </div>
        <button type="submit">Log</button>
      </form>
    </div>
  );
};

export default LogWaterIntake;
